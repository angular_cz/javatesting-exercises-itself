package cz.unittest.exercises.availability;

public interface BorrowingInterval {

  double getAverage();

  double getRange();

}

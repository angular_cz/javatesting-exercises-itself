package cz.unittest.exercises.domain;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class PenaltyDefinition {

  private Map<PublicationType, Integer> definition;

  public PenaltyDefinition() {
    definition = new HashMap<PublicationType, Integer>();
    definition.put(PublicationType.MAGAZINE, 10);
    definition.put(PublicationType.BOOK, 20);
    definition.put(PublicationType.DVD, 30);
  }

  public Integer getPenaltyForType(PublicationType type) {
    return definition.get(type);
  }

  public Integer getMaxItemPenalty() {
    return Collections.max(definition.values());
  }

  public Integer getMinItemPenalty() {
    return Collections.min(definition.values());
  }
}

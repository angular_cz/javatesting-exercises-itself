package cz.unittest.exercises.penalty;

import cz.unittest.exercises.domain.PublicationType;

public class OverdueItem {

  private PublicationType type;
  private int count;

  public OverdueItem(PublicationType type) {
    this(type, 1);
  }

  public OverdueItem(PublicationType type, int count) {
    this.type = type;
    this.count = count;
  }

  public PublicationType getType() {
    return type;
  }

  public int getCount() {
    return count;
  }
}

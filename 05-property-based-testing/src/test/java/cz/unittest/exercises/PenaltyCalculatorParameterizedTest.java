package cz.unittest.exercises;

import cz.unittest.exercises.domain.PublicationType;
import cz.unittest.exercises.penalty.OverdueItem;
import cz.unittest.exercises.penalty.PenaltyCalculator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

// TODO 1.1 Použijte parametrický runner
public class PenaltyCalculatorParameterizedTest {

  private PenaltyCalculator calculator;

  @Before
  public void prepareCalculator() {
    calculator = new PenaltyCalculator();
  }

  // TODO 1.2 Přidejte zdroj parametrů

  // TODO 1.3 Napište parametrický test penaltyByType

}


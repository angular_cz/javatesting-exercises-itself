package cz.unittest.exercises;

import static org.junit.Assume.*;
import static org.assertj.core.api.Assertions.assertThat;

import cz.unittest.exercises.domain.PenaltyDefinition;
import cz.unittest.exercises.domain.PublicationType;
import cz.unittest.exercises.penalty.PenaltyCalculator;
import cz.unittest.exercises.penalty.OverdueItem;
import org.junit.Before;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO 2.1 Použijte runner pro spouštění teorií

public class PenaltyCalculatorPropertyBasedTest {
  private PenaltyCalculator calculator;
  private PenaltyDefinition definition = new PenaltyDefinition();

  // TODO 2.3 Možné hodnoty typů publikací

  // TODO 2.5 Možné hodnoty počtu výpůjček

  @Before
  public void prepareCalculator() {
    calculator = new PenaltyCalculator();
  }

  @Theory
  public void minimalPricePerType(PublicationType type) {
    OverdueItem overdueItem = new OverdueItem(type);

    int result = calculator.calculate(overdueItem);

    assertThat(result).isGreaterThanOrEqualTo(definition.getMinItemPenalty());
  }

  // TODO 2.2 Teorie pro maximální hodnotu

  // TODO 2.4 Teorie pro výši pokuty při více položkách

  // TODO 3 Teorie pro výpočet kolekce upomínek

}

package cz.unittest.exercises.features.steps;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cz.unittest.exercises.WebDriverContainer;
import cz.unittest.exercises.po.ListItem;
import cz.unittest.exercises.po.ListPage;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class BeerListDefinitions {

  public static final String BASE_URI = "http://beer-app.angular.cz/";

  private ListPage listPage;
  private ListItem selectedRow;
  private WebDriver driver;

  @Before
  public void before() {
    driver = WebDriverContainer.getWebDriver();

    driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
  }

  @After
  public void after() {
    WebDriverContainer.quitDriver();
  }

  @Given("^beer-app list page$")
  public void beerAppListPage() throws Throwable {
    driver.get(BASE_URI);
    listPage = new ListPage(driver);
  }

  @Then("^list page title is \"([^\"]*)\"$")
  public void listPageTitleIs(String title) throws Throwable {
    assertThat(listPage.getTitle()).isEqualTo(title);
  }

  @When("^search for name (\\S+)")
  public void searchForNameRebel(String name) throws Throwable {
    listPage.setQuery(name);
  }

  @And("^search for degree (\\S+)$")
  public void searchForDegree(String degree) throws Throwable {
    listPage.setDegree(degree);
  }

  @Then("^there is row with name (\\S+)")
  public void thereIsRowWithNameRebel(String name) throws Throwable {
    selectedRow = listPage.getRowContaining(name);
    assertThat(selectedRow.getName()).isEqualTo(name);
  }

}

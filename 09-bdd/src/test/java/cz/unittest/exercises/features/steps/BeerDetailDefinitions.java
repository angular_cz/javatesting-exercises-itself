package cz.unittest.exercises.features.steps;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cz.unittest.exercises.WebDriverContainer;
import cz.unittest.exercises.po.Alerts;
import cz.unittest.exercises.po.DetailPage;
import cz.unittest.exercises.po.DetailRatingTab;
import cz.unittest.exercises.po.LoginModalDialog;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class BeerDetailDefinitions {

  public static final String BASE_URI = "http://beer-app.angular.cz/";

  private DetailPage detailPage;
  private DetailRatingTab ratingTab;
  private LoginModalDialog loginModal;
  private WebDriver driver;

  @Before
  public void before() {
    driver = WebDriverContainer.getWebDriver();

    driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
  }

  @After
  public void after() {
    WebDriverContainer.quitDriver();
  }

  @Given("^beer detail with id (\\d+)$")
  public void beerDetail(int id) throws Throwable {
    driver.get(BASE_URI + "#/beer/" + id );

    detailPage = new DetailPage(driver);
  }

  // TODO 3.2 Implementujte step definitions

  // TODO 3.4 Zjednodušující metoda
}

package cz.unittest.exercises.features.steps;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cz.unittest.exercises.WebDriverContainer;
import cz.unittest.exercises.po.LoginModalDialog;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class LoginDefinitions {

  public static final String BASE_URI = "http://beer-app.angular.cz/";

  private LoginModalDialog loginModal;
  private WebDriver driver;

  @Before
  public void before() {
    driver = WebDriverContainer.getWebDriver();

    driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
  }

  @After
  public void after() {
    WebDriverContainer.quitDriver();
  }

  @Given("^homepage$")
  public void homepage() throws Throwable {

    // TODO 2.2.1 Otevření domovské stránky

  }


  @When("^user shows login dialog$")
  public void showLoginDialog() throws Throwable {

    // TODO 2.2.2 Vyvolání dialogového okna

  }

  @And("^log with credentials \"([^\"]*)\" / \"([^\"]*)\"$")
  public void logWithCredentialsUserPass(String user, String pass) throws Throwable {

    // TODO 2.2.3 Přihlášení uživatele

  }

  @Then("^user is logged in as \"([^\"]*)\"$")
  public void userIsLoggedInAs(String name) throws Throwable {

    // TODO 2.2.4 Ověřte, že je uživatel přihlášen

  }

  // TODO 4.2 Vytvořte krok pro explicitní přihlášení

}

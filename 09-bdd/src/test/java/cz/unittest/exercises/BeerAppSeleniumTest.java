package cz.unittest.exercises;

import cz.unittest.exercises.po.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class BeerAppSeleniumTest {

  public static final String BASE_URI = "http://beer-app.angular.cz/";

  private WebDriver driver;

  @Before
  public void setUp() throws Exception {
    driver = WebDriverContainer.getWebDriver();
    driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

    driver.get(BASE_URI);
  }

  @After
  public void tearDown() throws Exception {
    WebDriverContainer.quitDriver();
  }

  @Test
  public void testHomepageTitle() throws InterruptedException {
    ListPage listPage = new ListPage(driver);
    assertThat(listPage.getTitle()).isEqualTo("Seznam piv");
  }

  @Test
  public void userCanLogIn() throws Exception {
    LoginModalDialog loginModal = LoginModalDialog.showByClick(driver);
    loginModal.logAs("user", "pass");

    assertThat(loginModal.getLoggedUser()).isEqualTo("user");
  }

  @Test
  public void userCanSearchForBeer() throws InterruptedException {

    ListPage listPage = new ListPage(driver);
    listPage.setDegree("11");
    listPage.setQuery("Rebel");

    ListItem selectedRow = listPage.getRowContaining("Rebel");
    assertThat(selectedRow.getName()).isEqualTo("Rebel");
  }

  @Test
  public void ratingBeerWithImplicitLogin() throws Exception {
    LoginModalDialog loginModal = LoginModalDialog.showByClick(driver);
    loginModal.logAs("user", "pass");

    driver.get(BASE_URI + "#/beer/1");

    DetailPage detailPage = new DetailPage(driver);
    DetailRatingTab ratingTab = detailPage.goToRatingTab();

    ratingTab.setName("user");
    ratingTab.setRating(3);
    ratingTab.setDescription("description");
    ratingTab.clickRateButton();

    Alerts alerts = new Alerts(driver);
    assertThat(alerts.getAlertText()).contains("Vaše hodnocení bylo přidáno, děkujeme");
  }

  @Test
  public void ratingBeerWithExplicitLogin() throws Exception {

    driver.get(BASE_URI + "#/beer/1");

    DetailPage detailPage = new DetailPage(driver);
    DetailRatingTab ratingTab = detailPage.goToRatingTab();

    ratingTab.setName("user");
    ratingTab.setRating(3);
    ratingTab.setDescription("description");
    ratingTab.clickRateButton();

    LoginModalDialog loginModal = new LoginModalDialog(driver);
    loginModal.logAs("user", "pass");

    Alerts alerts = new Alerts(driver);
    assertThat(alerts.getAlertText()).contains("Vaše hodnocení bylo přidáno, děkujeme");
  }

}

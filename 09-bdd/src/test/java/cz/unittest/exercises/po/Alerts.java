package cz.unittest.exercises.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Alerts {

  private final WebElement alerts;
  private WebDriver driver;
  private final WebDriverWait wait;

  public Alerts(WebDriver driver) {
    this.driver = driver;

    wait = new WebDriverWait(driver, 15);
    alerts = wait.until(ExpectedConditions.presenceOfElementLocated(By.tagName("notifications")));
  }

  public String getAlertText() {
    WebElement alert = wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(alerts, By.cssSelector("div.alert")));

    return alert.getText();
  }
}


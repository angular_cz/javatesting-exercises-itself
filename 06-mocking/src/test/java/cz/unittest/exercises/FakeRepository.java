package cz.unittest.exercises;

import cz.unittest.exercises.domain.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FakeRepository implements BorrowingsRepository {

  public static User USER1, USER2, USER3;
  public static Publication PUBLICATION1, PUBLICATION2, PUBLICATION3, PUBLICATION4;

  static {
    USER1 = new User(1, "Josef Novák");
    USER2 = new User(2, "Petr Pavlíček");
    USER3 = new User(3, "Pavel Bílek");

    PUBLICATION1 = new Publication(PublicationType.BOOK, "Clean Code");
    PUBLICATION2 = new Publication(PublicationType.BOOK, "Refactoring: Improving the Design of Existing Code");
    PUBLICATION3 = new Publication(PublicationType.BOOK, "Java 8: The Fundamentals");
    PUBLICATION4 = new Publication(PublicationType.MAGAZINE, "Echo 24");
  }

  private final ArrayList<Borrowing> borrowings;

  public FakeRepository() {
    borrowings = new ArrayList<Borrowing>();

    borrowings.add(new Borrowing(USER1, PUBLICATION1));
    borrowings.add(new Borrowing(USER1, PUBLICATION2));
    borrowings.add(new Borrowing(USER2, PUBLICATION3));
    borrowings.add(new Borrowing(USER3, PUBLICATION4));
    borrowings.add(new Borrowing(USER3, PUBLICATION1));
  }

  public List<Borrowing> getOverdueBorrowings() {
    return borrowings;
  }

  public List<GroupedBorrowings> getGroupedOverdueBorrowings() {
    HashMap<User, GroupedBorrowings> userMap = new HashMap<User, GroupedBorrowings>();

    for (Borrowing borrowing : this.getOverdueBorrowings()) {
      User user = borrowing.getUser();

      if (!userMap.containsKey(user)) {
        GroupedBorrowings groupedBorrowings = new GroupedBorrowings(user, new ArrayList<Borrowing>());
        userMap.put(user, groupedBorrowings);
      }

      userMap.get(user).getBorrowings().add(borrowing);
    }

    return new ArrayList<GroupedBorrowings>(userMap.values());
  }
}

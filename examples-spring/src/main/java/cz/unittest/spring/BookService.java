package cz.unittest.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BookService {

  @Autowired
  private BookRepository bookRepository;

  public Book findOne(Long id) {
    return bookRepository.findOne(id);
  }

  public Book create(Book book) {
    return bookRepository.save(book);
  }
}

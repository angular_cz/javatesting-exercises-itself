package cz.unittest.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.MimeMappings;

@SpringBootApplication
public class Application implements EmbeddedServletContainerCustomizer {

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

  public void customize(ConfigurableEmbeddedServletContainer container) {
    MimeMappings mappings = new MimeMappings(MimeMappings.DEFAULT);
    mappings.add("html", "text/html;charset=utf-8");

    container.setMimeMappings(mappings );
  }
}

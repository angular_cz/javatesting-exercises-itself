package cz.unittest.spring.rest;

import cz.unittest.spring.Book;
import cz.unittest.spring.BookService;
import cz.unittest.spring.LibrarySearchEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class LibraryEndpoint {

  @Autowired
  BookService bookService;

  @Autowired
  LibrarySearchEngine searchEngine;

  @GetMapping("/book/{id}")
  public Book getById(@PathVariable(value="id") Long id) {
    return bookService.findOne(id);
  }

  @PostMapping("/book")
  public Book createNew(@RequestBody Book book) {
    return bookService.create(book);
  }

  @GetMapping("/search")
  public Book findBook(@RequestParam(name = "name") String name) {
    return searchEngine.getBooks(name);
  }
}

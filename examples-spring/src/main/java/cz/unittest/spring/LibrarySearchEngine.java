package cz.unittest.spring;

import org.springframework.stereotype.Component;

@Component
public class LibrarySearchEngine {

  private BookRepository bookRepository;

  public LibrarySearchEngine(BookRepository bookRepository) {
    this.bookRepository = bookRepository;
  }

  public Book getBooks(String name) {
    return bookRepository.findOneByName(name);
  }
}

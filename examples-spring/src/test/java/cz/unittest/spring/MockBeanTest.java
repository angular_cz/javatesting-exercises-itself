package cz.unittest.spring;

import cz.unittest.spring.rest.LibraryEndpoint;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {Application.class,
                                 MockBeanTest.TestConfig.class})
public class MockBeanTest {

  @MockBean
  LibrarySearchEngine searchEngine;

  @Configuration
  static class TestConfig {
    @Bean
    public BookService bookService() {
      return mock(BookService.class);
    }
  }

  @Autowired
  private LibraryEndpoint libraryEndpoint;

  @Autowired
  private BookService bookService;

  @Test
  public void searchEngineShouldBeMock() throws Exception {
    Book book = new Book();
    when(searchEngine.getBooks("test")).thenReturn(book);

    assertEquals(book, searchEngine.getBooks("test"));
  }

  @Test
  public void booksServiceShouldBeMock() throws Exception {
    libraryEndpoint.getById(1L);

    verify(bookService).findOne(1L);
  }
}

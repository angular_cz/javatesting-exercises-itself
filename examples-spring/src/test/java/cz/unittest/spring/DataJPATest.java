package cz.unittest.spring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@Sql({"classpath:test-db.sql"})
public class DataJPATest {

  @Autowired
  private BookRepository bookRepository;

  @Autowired
  private TestEntityManager entityManager;

  @Test
  public void thereArePopulatedData() throws Exception {
    Book guide = bookRepository.findOne(1L);
    assertThat(guide.getName()).isEqualTo("Guide to galaxy");
  }

  @Test
  public void entityManagerShouldBeAbleToSave() {
    Book entity = new Book("Created by entity manager", "Author", 1985);
    entityManager.persist(entity);

    Book book = bookRepository.findOne(entity.getId());
    assertThat(book.getName()).isEqualTo("Created by entity manager");
    assertThat(book).isEqualTo(entity);
  }

}

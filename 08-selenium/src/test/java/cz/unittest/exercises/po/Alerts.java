package cz.unittest.exercises.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Alerts {

  private final WebDriverWait wait;

  @FindBy(tagName = "notifications")
  private WebElement alerts;

  public Alerts(WebDriver driver) {
    PageFactory.initElements(driver, this);

    wait = new WebDriverWait(driver, 15);
  }

  public String getAlertText() {
    WebElement alert = wait.until(
      ExpectedConditions.presenceOfNestedElementLocatedBy(alerts, By.cssSelector("div.alert>strong")));

    return alert.getText();
  }
}


package cz.unittest.exercises.po;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DetailRatingTab {

  private WebElement ratings;

  public DetailRatingTab(WebDriver driver) {

    // TODO 3.2 Nalezení tabu hodnocení elementu
  }

  public void setName(String name) {

    // TODO 3.3 Metoda pro nastavení jména
  }

  public void setDescription(String text) {

    // TODO 3.4 Metoda pro nastavení popisu
  }

  public void setRating(int rating) {

    // TODO 3.5 Metoda pro nastavení hodnocení
  }

  public void clickRateButton() {

    // TODO 3.6 Metoda pro odeslání hodnocení
  }

}

package cz.unittest.exercises.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ListItem {

  private WebElement row;
  private WebDriver driver;

  public ListItem(WebElement row, WebDriver driver) {
    this.row = row;
    this.driver = driver;
  }

  public String getName() {
    WebElement name = row.findElement(By.cssSelector("td:first-child"));
    return name.getText();
  }

  public DetailPage goToDetail() {
    WebElement detailButton = row.findElement(By.partialLinkText("Detail"));
    detailButton.click();

    return new DetailPage(driver);
  }
}

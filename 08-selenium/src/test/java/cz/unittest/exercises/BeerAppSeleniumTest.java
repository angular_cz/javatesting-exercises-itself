package cz.unittest.exercises;

import cz.unittest.exercises.po.*;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class BeerAppSeleniumTest {

  public static final String BASE_URI = "http://beer-app.angular.cz/";

  @Rule
  public WebDriverRule webDriverRule = new WebDriverRule();
  private WebDriver driver;

  @Before
  public void setUp() throws Exception {
    driver = webDriverRule.getDriver();
    driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

    driver.get(BASE_URI);
  }

  @Test
  public void testHomepageTitle() throws InterruptedException {
    ListPage listPage = new ListPage(driver);
    assertThat(listPage.getTitle()).isEqualTo("Seznam piv");
  }

  // TODO 2.1 Vytvořte test searchRebel11AndGoToTheDetail

  // TODO 3.7 Vytvořte test ratingBeerWithExplicitLogin

}

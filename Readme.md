# Testování z pohledu vývojáře - Java

## Co si nainstalovat

 - jdk 1.7 nebo vyšší
 - maven
 - IDE s podporou spouštění testů 
    - doporučujeme Jetbrains InteliJ Idea
    - Eclipse, Netbeans pokud jsou vašim primárním editorem

### Rozchození cvičení

Stáhněte si repozitář

```
git clone https://bitbucket.org/angular_cz/javatesting-exercises-itself

```

Importujte projekt do vývojového prostředí.

Spusťte maven build v IDE nebo příkazové řádce

```
mvn clean install
```

Spusťte test ve cvičení 01-testing. Měl by procházet
 

Pokud vše dopadne dobře jste připraveni
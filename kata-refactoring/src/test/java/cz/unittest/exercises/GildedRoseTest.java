package cz.unittest.exercises;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

import org.assertj.core.api.Assertions;
import org.junit.Ignore;
import org.junit.Test;

public class GildedRoseTest {

  @Test
  @Ignore
  public void testItem() {
    Item[] items = new Item[] { new Item("Aged Brie", 2, 0) };

    GildedRose app = new GildedRose(items);
    app.updateQuality();

    Item expectedItem = new Item("Aged Brie", 0, 0);
    Item updatedItem = app.items[0];

    fail("there is no assertion");
  }

}

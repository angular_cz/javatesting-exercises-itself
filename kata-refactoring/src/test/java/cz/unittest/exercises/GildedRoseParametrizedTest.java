package cz.unittest.exercises;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public class GildedRoseParametrizedTest {

  private Item original;
  private Item expected;

  public GildedRoseParametrizedTest(Item original, Item expected) {
    this.original = original;
    this.expected = expected;
  }

  @Parameterized.Parameters(name = "testItem {0}")
  public static Object[][] parameters() throws Exception {

    return new Object[][]{
      {
        new Item("+5 Dexterity Vest", 10, 20),
        new Item("+5 Dexterity Vest", 9, 19)
      },
      {
        new Item("Aged Brie", 2, 0),
        new Item("Aged Brie", 1, 1)
      },
      {
        new Item("Elixir of the Mongoose", 5, 7),
        new Item("Elixir of the Mongoose", 4, 6)
      },
      {
        new Item("Sulfuras, Hand of Ragnaros", 0, 80),
        new Item("Sulfuras, Hand of Ragnaros", 0, 80)
      },
      {
        new Item("Sulfuras, Hand of Ragnaros", -1, 80),
        new Item("Sulfuras, Hand of Ragnaros", -1, 80)
      },
      {
        new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
        new Item("Backstage passes to a TAFKAL80ETC concert", 14, 21)
      },
      {
        new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
        new Item("Backstage passes to a TAFKAL80ETC concert", 9, 50)
      },
      {
        new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49),
        new Item("Backstage passes to a TAFKAL80ETC concert", 4, 50)
      }
    };
  }

  @Test
  public void testItem() {
    Item[] items = new Item[]{original};
    GildedRose app = new GildedRose(items);
    app.updateQuality();

    Item modifiedItem = app.items[0];

    assertThat(modifiedItem)
      .isEqualToComparingFieldByField(expected);
  }
}

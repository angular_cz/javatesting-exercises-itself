package cz.unittest.exercises;


import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.Rule;

import static org.junit.Assert.assertEquals;

public class UserTest {

  private static final LocalDate DAY_WHEN_USER_HAS_BIRTHDAY = new LocalDate(2016, 5, 15);

  private static final LocalDate FIRST_DAY_IN_BIRTH_DATE_MONTH = DAY_WHEN_USER_HAS_BIRTHDAY.withDayOfMonth(1);
  private static final LocalDate LAST_DAY_IN_BIRTH_DATE_MONTH = DAY_WHEN_USER_HAS_BIRTHDAY.withDayOfMonth(30);

  private static final LocalDate ONE_MONTH_BEFORE_BIRTH_DATE = DAY_WHEN_USER_HAS_BIRTHDAY.minusMonths(1);
  private static final LocalDate ONE_MONTH_AFTER_BIRTH_DATE = DAY_WHEN_USER_HAS_BIRTHDAY.plusMonths(1);

  private static final LocalDate DATE_OF_BIRTH = DAY_WHEN_USER_HAS_BIRTHDAY.minusYears(41);

  private User user;

  @Before
  public void prepareUser() {
    user = new User("Test user", DATE_OF_BIRTH);
  }

  @Test
  public void ageOneMonthBeforeBirthDayShouldBeUnchanged() {
    assertEquals(40, user.getAgeInThisMonth(ONE_MONTH_BEFORE_BIRTH_DATE));
  }

  @Test
  public void ageOneMonthAfterBirthDayShouldBeHigher() {
    assertEquals(41, user.getAgeInThisMonth(ONE_MONTH_AFTER_BIRTH_DATE));
  }

  @Test
  public void ageInMonthOfBirthShouldBeConsistent() {

    assertEquals(41, user.getAgeInThisMonth(DAY_WHEN_USER_HAS_BIRTHDAY));
    assertEquals(41, user.getAgeInThisMonth(FIRST_DAY_IN_BIRTH_DATE_MONTH));
    assertEquals(41, user.getAgeInThisMonth(LAST_DAY_IN_BIRTH_DATE_MONTH));

  }

  // TODO - 2.1 Testujte vyhození výjimky - shouldThrowMinimalAgeExceptionForTooYoungPerson

  // TODO - 2.2 Testujte vyhození výjimky pomocí ExpectedException - shouldThrowMinimalAgeExceptionForTooYoungPersonWithRightMessage

}

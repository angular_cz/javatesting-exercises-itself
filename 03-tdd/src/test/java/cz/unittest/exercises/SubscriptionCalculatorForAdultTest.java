package cz.unittest.exercises;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SubscriptionCalculatorForAdultTest extends AbstractSubscriptionCalculatorTest {

  private User adultUser;

  @Before
  public void prepareAdultUser() {
    adultUser = UserTestUtil.createUser(new LocalDate(1980, 1, 1));
  }

  @Test
  public void calculatePrice_withAdult_shouldReturnBasePrice() {
    int annualPrice = calculateAnnualPriceFor(adultUser);

    assertEquals(BASE_PRICE, annualPrice);
  }

  @Test
  public void calculatePrice_withAdultForSixMonths_shouldReturnHalfPrice() {
    int sixMonthsPrice = calculateSixMonthsPriceFor(adultUser);

    assertEquals(BASE_PRICE / 2, sixMonthsPrice);
  }

  // TODO 4.4 Test tříměsíčního předplatného pro dospělé - calculatePrice_withAdultForThreeMonth_shouldBeHalfPrice

}

package cz.unittest.exercises.rules;

import cz.unittest.exercises.Duration;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ChildRuleTest extends AbstractRuleTest {

  @Before
  public void setUp() throws Exception {
    rule = new ChildRule();
  }

  @Test
  public void conditionsForRuleShouldBeApplied() throws Exception {
    assertTrue(rule.canBeAppliedFor(child, Duration.ANNUAL));
  }

  @Test
  public void conditionsForRuleShouldNotBeApplied() throws Exception {
    assertFalse(rule.canBeAppliedFor(retired, Duration.ANNUAL));
    assertFalse(rule.canBeAppliedFor(adult, Duration.ANNUAL));
    assertFalse(rule.canBeAppliedFor(young, Duration.ANNUAL));
  }

  @Test
  public void priceShouldBeFree() throws Exception {
    assertEquals(0, rule.apply(100));
  }
}

package cz.unittest.exercises.rules;

import cz.unittest.exercises.Duration;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RetiredRuleTest extends AbstractRuleTest {

  @Before
  public void setUp() throws Exception {
    rule = new RetiredRule();
  }

  @Test
  public void conditionsForRuleShouldBeApplied() throws Exception {
    assertTrue(rule.canBeAppliedFor(retired, Duration.ANNUAL));
  }

  @Test
  public void conditionsForRuleShouldNotBeApplied() throws Exception {
    assertFalse(rule.canBeAppliedFor(child, Duration.ANNUAL));
    assertFalse(rule.canBeAppliedFor(adult, Duration.ANNUAL));
    assertFalse(rule.canBeAppliedFor(young, Duration.ANNUAL));
  }

  @Test
  public void priceShouldBeFree() throws Exception {
    assertEquals(0, rule.apply(100));
  }
}

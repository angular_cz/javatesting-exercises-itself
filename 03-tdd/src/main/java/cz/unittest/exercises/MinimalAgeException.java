package cz.unittest.exercises;

public class MinimalAgeException extends RuntimeException {

  public MinimalAgeException(String message) {
    super(message);
  }
}

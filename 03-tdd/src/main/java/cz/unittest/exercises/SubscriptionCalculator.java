package cz.unittest.exercises;

import cz.unittest.exercises.rules.*;
import org.joda.time.LocalDate;

import java.util.ArrayList;

public class SubscriptionCalculator {

  private int basePrice;
  private ArrayList<CalculationRule> calculationRules;

  public SubscriptionCalculator(int basePrice) {
    this.basePrice = basePrice;

    calculationRules = new ArrayList<CalculationRule>();
    calculationRules.add(new RetiredRule());
    calculationRules.add(new ChildRule());
    calculationRules.add(new YoungRule());

    // TODO 2.1 Přidejte nové pravidlo do kolekce
    // TODO 4.3 Přidejte nové pravidlo do kolekce
  }

  public int calculatePrice(User user, Duration duration) {

    int price = basePrice;

    // TODO 2.2 Implementujte výpočet pomocí pravidel

    int age = user.getAgeInThisMonth(new LocalDate());

    if (duration == Duration.SIX_MONTHS) {
      price = price / 2;
    }

    if (age <= 25 && age >= 15) {
      price = price / 2;
    }

    if (age >= 70 || age < 15) {
      price = 0;
    }

    return price;
  }
}

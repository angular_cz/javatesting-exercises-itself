package cz.unittest.exercises.rules;

import cz.unittest.exercises.Duration;
import cz.unittest.exercises.User;
import org.joda.time.LocalDate;

public class YoungRule implements CalculationRule{

  public boolean canBeAppliedFor(User user, Duration duration) {
    int ageInThisMonth = user.getAgeInThisMonth(new LocalDate());
    return ageInThisMonth <= 25 && ageInThisMonth >= 15;
  }

  public int apply(int price) {
    return price / 2;
  }
}

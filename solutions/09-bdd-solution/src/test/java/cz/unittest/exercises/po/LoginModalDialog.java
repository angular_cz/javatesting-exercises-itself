package cz.unittest.exercises.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginModalDialog {

  private final WebElement dialog;
  private final WebDriverWait wait;

  public static LoginModalDialog showByClick(WebDriver driver) {

    WebDriverWait wait = new WebDriverWait(driver, 10);
    WebElement loginButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button[auth-login-link]")));
    loginButton.click();

    return new LoginModalDialog(driver);
  }

  public LoginModalDialog(WebDriver driver) {

    wait = new WebDriverWait(driver, 10);
    dialog = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.modal-dialog")));
  }

  public void logAs(String name, String pass) {
    WebElement nameElement = dialog.findElement(By.id("inputName"));
    nameElement.clear();
    nameElement.sendKeys(name);

    WebElement passElement = dialog.findElement(By.id("inputPass"));
    passElement.clear();
    passElement.sendKeys(pass);

    WebElement button = dialog.findElement(By.cssSelector("button[type='submit']"));
    button.click();
  }

  public String getLoggedUser() {
    WebElement loggedUser = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("a[auth-user-name]")));
    return loggedUser.getText();
  }
}

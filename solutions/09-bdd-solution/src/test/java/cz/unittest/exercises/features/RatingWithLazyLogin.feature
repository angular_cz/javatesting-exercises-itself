Feature: User can rate beer without explicit loging

  Scenario: User can rate beer then login
    Given beer detail with id 1
    When user clicks on rating tab
    And fill rating
    And click add rating
    And login dialog is shown
    And log with credentials "user" / "pass"
    Then rating is successfully created

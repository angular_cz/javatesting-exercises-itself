package cz.unittest.exercises;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class WebDriverContainer {
  private static WebDriver driver;

  public static WebDriver getWebDriver() {
    if(driver == null) {
      ChromeDriverManager.getInstance().setup();

      driver = new ChromeDriver();
      driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
    }
    return driver;
  };

  public static void quitDriver() {
    if(driver == null) {
      return;
    }

    driver.quit();
    driver = null;
  }
}

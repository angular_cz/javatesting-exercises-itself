Feature: Can search for beer on list page

  Scenario: Can go to search page
    Given beer-app list page
    Then list page title is "Seznam piv"

  Scenario: Can found record Rebel
    Given beer-app list page
    When search for name Rebel
    And search for degree 11
    Then there is row with name Rebel

  Scenario Outline: search
    Given beer-app list page
    When search for name <name>
    And search for degree <degree>
    Then there is row with name <whole-name>

    Examples:
      |   name      | degree  |   whole-name  |
      |  Janá       |    Dia  |   Janáček     |
      |  Rebel      |    11   |   Rebel       |
      |  Radegast   |    18   |   Radegast    |


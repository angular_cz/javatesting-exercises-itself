package cz.unittest.exercises.features.steps;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cz.unittest.exercises.WebDriverContainer;
import cz.unittest.exercises.po.Alerts;
import cz.unittest.exercises.po.DetailPage;
import cz.unittest.exercises.po.DetailRatingTab;
import cz.unittest.exercises.po.LoginModalDialog;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class BeerDetailDefinitions {

  public static final String BASE_URI = "http://beer-app.angular.cz/";

  private DetailPage detailPage;
  private DetailRatingTab ratingTab;
  private WebDriver driver;

  @Before
  public void before() {
    driver = WebDriverContainer.getWebDriver();

    driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
  }

  @After
  public void after() {
    WebDriverContainer.quitDriver();
  }

  @Given("^beer detail with id (\\d+)$")
  public void beerDetail(int id) throws Throwable {
    driver.get(BASE_URI + "#/beer/" + id );

    detailPage = new DetailPage(driver);
  }

  @When("^user clicks on rating tab$")
  public void userClicksOnRatingTab() throws Throwable {
    ratingTab = detailPage.goToRatingTab();
  }

  @And("^writes name \"([^\"]*)\"$")
  public void writesName(String name) throws Throwable {
    ratingTab.setName(name);
  }

  @And("^selects (\\d+) stars$")
  public void selectsStars(int rate) throws Throwable {
    ratingTab.setRating(rate);
  }

  @And("^writes description$")
  public void writesDescription() throws Throwable {
    ratingTab.setDescription("description");
  }

  @And("^click add rating$")
  public void clickAddRating() throws Throwable {
    ratingTab.clickRateButton();
  }

  @Then("^rating is successfully created$")
  public void ratingIsSuccessfullyCreated() throws Throwable {
    Alerts alerts = new Alerts(driver);
    assertThat(alerts.getAlertText()).contains("Vaše hodnocení bylo přidáno, děkujeme");
  }

  @And("^fill rating$")
  public void fillRating() throws Throwable {
    writesName("Petr");
    selectsStars(5);
    writesDescription();
  }
}

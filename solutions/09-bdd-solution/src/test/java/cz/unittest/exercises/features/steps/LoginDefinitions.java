package cz.unittest.exercises.features.steps;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cz.unittest.exercises.WebDriverContainer;
import cz.unittest.exercises.po.LoginModalDialog;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class LoginDefinitions {

  public static final String BASE_URI = "http://beer-app.angular.cz/";

  private LoginModalDialog loginModal;
  private WebDriver driver;

  @Before
  public void before() {
    driver = WebDriverContainer.getWebDriver();

    driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
  }

  @After
  public void after() {
    WebDriverContainer.quitDriver();
  }

  @Given("^homepage$")
  public void homepage() throws Throwable {

    driver.get(BASE_URI);
  }

  @When("^user shows login dialog$")
  public void showLoginDialog() throws Throwable {

    loginModal = LoginModalDialog.showByClick(driver);
  }

  @And("^log with credentials \"([^\"]*)\" / \"([^\"]*)\"$")
  public void logWithCredentialsUserPass(String user, String name) throws Throwable {

    loginModal.logAs(user, name);
  }

  @Then("^user is logged in as \"([^\"]*)\"$")
  public void userIsLoggedInAs(String name) throws Throwable {

    assertThat(loginModal.getLoggedUser()).isEqualTo(name);
  }

  @And("^login dialog is shown$")
  public void loginDialogIsShown() throws Throwable {
    loginModal = new LoginModalDialog(driver);
  }

}

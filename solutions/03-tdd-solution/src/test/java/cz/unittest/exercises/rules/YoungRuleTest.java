package cz.unittest.exercises.rules;

import cz.unittest.exercises.Duration;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class YoungRuleTest extends AbstractRuleTest {

  @Before
  public void setUp() throws Exception {
    rule = new YoungRule();
  }

  @Test
  public void conditionsForRuleShouldBeApplied() throws Exception {
    assertTrue(rule.canBeAppliedFor(young, Duration.ANNUAL));
  }

  @Test
  public void conditionsForRuleShouldNotBeApplied() throws Exception {
    assertFalse(rule.canBeAppliedFor(retired, Duration.ANNUAL));
    assertFalse(rule.canBeAppliedFor(child, Duration.ANNUAL));
    assertFalse(rule.canBeAppliedFor(adult, Duration.ANNUAL));
  }

  @Test
  public void priceShouldBeHalf() throws Exception {
    assertEquals(50, rule.apply(100));
  }
}

package cz.unittest.exercises.rules;

import cz.unittest.exercises.Duration;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FreeYoungRuleTest extends AbstractRuleTest {

  @Before
  public void setUp() throws Exception {
    rule = new FreeYoungRule();
  }

  @Test
  public void conditionsForRuleShouldBeApplied() throws Exception {
    assertTrue(rule.canBeAppliedFor(young, Duration.THREE_MONTHS));
  }

  @Test
  public void conditionsForRuleShouldNotBeApplied() throws Exception {
    assertFalse(rule.canBeAppliedFor(young, Duration.ANNUAL));
    assertFalse(rule.canBeAppliedFor(young, Duration.SIX_MONTHS));

    assertFalse(rule.canBeAppliedFor(retired, Duration.THREE_MONTHS));
    assertFalse(rule.canBeAppliedFor(adult, Duration.THREE_MONTHS));
    assertFalse(rule.canBeAppliedFor(child, Duration.THREE_MONTHS));
  }

  @Test
  public void priceShouldBeFree() throws Exception {
    assertEquals(0, rule.apply(100));
  }
}

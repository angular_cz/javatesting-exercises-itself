package cz.unittest.exercises;

import org.joda.time.LocalDate;
import org.junit.Before;

public abstract class AbstractSubscriptionCalculatorTest {

  private SubscriptionCalculator calculator;
  public static final int BASE_PRICE = 100;

  @Before
  public void prepareCalculator() {
    calculator = new SubscriptionCalculator(BASE_PRICE);
  }

  protected int calculateAnnualPriceFor(User user) {
    return calculator.calculatePrice(user, Duration.ANNUAL);
  }

  protected int calculateSixMonthsPriceFor(User user) {
    return calculator.calculatePrice(user, Duration.SIX_MONTHS);
  }

  protected int calculateThreeMonthsPriceFor(User user) {
    return calculator.calculatePrice(user, Duration.THREE_MONTHS);
  }

}

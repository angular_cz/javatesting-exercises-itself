package cz.unittest.exercises.rules;

import cz.unittest.exercises.Duration;
import cz.unittest.exercises.User;

public class FreeYoungRule extends YoungRule {

  public boolean canBeAppliedFor(User user, Duration duration) {
    return duration == Duration.THREE_MONTHS && super.canBeAppliedFor(user, duration);
  }

  public int apply(int price) {
    return 0;
  }
}

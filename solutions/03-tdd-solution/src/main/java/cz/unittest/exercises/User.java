package cz.unittest.exercises;

import org.joda.time.LocalDate;

public class User {

  private String name;
  private LocalDate dateOfBirth;

  public User(String name, LocalDate dateOfBirth){
    this.name = name;
    this.dateOfBirth = dateOfBirth;

    if (getAgeInThisMonth(new LocalDate()) < 2) {
      throw new MinimalAgeException("User must be at least 2 years old.");
    }
  }

  public String getName() {
    return name;
  }

  public int getAgeInThisMonth(LocalDate today) {

    int age = today.getYear() -  dateOfBirth.getYear();
    if (dateOfBirth.getMonthOfYear() > today.getMonthOfYear()) {
      age--;
    }

    return age;
  }

}

package cz.unittest.exercises;

public enum Duration {
  ANNUAL, SIX_MONTHS, THREE_MONTHS
}

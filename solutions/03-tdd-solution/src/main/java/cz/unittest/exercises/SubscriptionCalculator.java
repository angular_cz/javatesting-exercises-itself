package cz.unittest.exercises;

import cz.unittest.exercises.rules.*;

import java.util.ArrayList;

public class SubscriptionCalculator {

  private int basePrice;
  private final ArrayList<CalculationRule> calculationRules;

  public SubscriptionCalculator(int basePrice) {
    this.basePrice = basePrice;

    calculationRules = new ArrayList<CalculationRule>();
    calculationRules.add(new HalfPriceDurationRule());
    calculationRules.add(new YoungRule());
    calculationRules.add(new FreeYoungRule());
    calculationRules.add(new RetiredRule());
    calculationRules.add(new ChildRule());
  }

  public int calculatePrice(User user, Duration duration) {

    int price = basePrice;

    for (CalculationRule rule : calculationRules) {
      if (rule.canBeAppliedFor(user, duration)) {
        price = rule.apply(price);
      }
    }

    return price;
  }
}

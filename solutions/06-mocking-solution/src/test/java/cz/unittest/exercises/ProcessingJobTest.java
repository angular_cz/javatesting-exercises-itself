package cz.unittest.exercises;

import cz.unittest.exercises.domain.Publication;
import cz.unittest.exercises.domain.User;
import cz.unittest.exercises.reminder.ReminderContent;
import cz.unittest.exercises.reminder.ReminderQueue;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class ProcessingJobTest {

  private ProcessingJob processingJob;
  private ReminderQueue reminderQueueMock;

  @Before
  public void setUp() throws Exception {
    BorrowingsRepository repository = new FakeRepository();
    reminderQueueMock = mock(ReminderQueue.class);
    processingJob = new ProcessingJob(repository, reminderQueueMock);
  }

  @Test
  public void jobShouldSendReminderForOverdueBorrowings() {
    int numberOfReminders = processingJob.process();
    assertThat(numberOfReminders).isEqualTo(3);

    verify(reminderQueueMock, times(3)).add(any(User.class), any(ReminderContent.class));

    verify(reminderQueueMock, times(1)).add(eq(FakeRepository.USER1), any(ReminderContent.class));
    verify(reminderQueueMock, times(1)).add(eq(FakeRepository.USER2), any(ReminderContent.class));
    verify(reminderQueueMock, times(1)).add(eq(FakeRepository.USER3), any(ReminderContent.class));
  }

  @Test
  public void user1ShouldReportPublication1AndPublication2() {
    processingJob.process();

    ArgumentCaptor<ReminderContent> captor = ArgumentCaptor.forClass(ReminderContent.class);

    verify(reminderQueueMock).add(eq(FakeRepository.USER1), captor.capture());

    ReminderContent content = captor.getValue();
    assertThat(content.getBorrowings())
      .extracting("publication", Publication.class)
      .containsOnly(FakeRepository.PUBLICATION1, FakeRepository.PUBLICATION2);
  }

}

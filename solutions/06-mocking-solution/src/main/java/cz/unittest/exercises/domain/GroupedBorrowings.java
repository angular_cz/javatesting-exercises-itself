package cz.unittest.exercises.domain;

import java.util.List;

public class GroupedBorrowings {
  private User user;
  private List<Borrowing> borrowings;

  public GroupedBorrowings(User user, List<Borrowing> borrowings) {
    this.user = user;
    this.borrowings = borrowings;
  }

  public User getUser() {
    return user;
  }

  public List<Borrowing> getBorrowings() {
    return borrowings;
  }
}

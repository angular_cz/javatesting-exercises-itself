package cz.unittest.exercises.reminder;

import cz.unittest.exercises.domain.User;

public class ReminderQueue {

  public void add(User user, ReminderContent request) {
    System.out.println("Send mail reminder for user " + user.getName() + ": " + request.getBorrowings());
  }
}

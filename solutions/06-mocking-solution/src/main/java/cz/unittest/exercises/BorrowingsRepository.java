package cz.unittest.exercises;

import cz.unittest.exercises.domain.Borrowing;
import cz.unittest.exercises.domain.GroupedBorrowings;

import java.util.List;

public interface BorrowingsRepository {
  List<Borrowing> getOverdueBorrowings();

  List<GroupedBorrowings> getGroupedOverdueBorrowings();
}

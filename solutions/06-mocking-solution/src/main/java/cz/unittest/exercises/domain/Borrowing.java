package cz.unittest.exercises.domain;

public class Borrowing {

  private Publication publication;
  private User user;

  public Borrowing(User user, Publication publication) {
    this.user = user;
    this.publication = publication;
  }

  public User getUser() {
    return user;
  }

  public Publication getPublication() {
    return publication;
  }

  @Override
  public String toString() {
    return getPublication().toString();
  }
}

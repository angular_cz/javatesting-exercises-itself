package cz.unittest.exercises.availability;

public class BorrowingIntervalImpl implements BorrowingInterval {

  private double average;
  private double range;

  public BorrowingIntervalImpl(double average, double range) {

    this.average = average;
    this.range = range;
  }

  public BorrowingInterval applyUserCoefficient(double userCoefficient) {
    return new BorrowingIntervalImpl(average * userCoefficient, this.range);
  }

  public double getAverage() {
    return average;
  }

  public double getRange() {
    return range;
  }
}

package cz.unittest.exercises.stats;


import cz.unittest.exercises.Subject;
import cz.unittest.exercises.User;
import cz.unittest.exercises.availability.BorrowingInterval;

public interface StatsRepository {

  BorrowingInterval getAverageBorrowingTerm(Subject subject);

  double getUserCoefficient(User user);
}

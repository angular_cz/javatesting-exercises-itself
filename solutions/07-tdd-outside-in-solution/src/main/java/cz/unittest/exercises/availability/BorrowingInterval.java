package cz.unittest.exercises.availability;

public interface BorrowingInterval {
  BorrowingInterval applyUserCoefficient(double userCoefficient);

  double getAverage();

  double getRange();
}

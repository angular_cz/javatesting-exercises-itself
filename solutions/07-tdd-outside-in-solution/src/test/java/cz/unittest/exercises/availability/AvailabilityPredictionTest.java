package cz.unittest.exercises.availability;

import cz.unittest.exercises.Subject;
import cz.unittest.exercises.User;
import cz.unittest.exercises.stats.StatsRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AvailabilityPredictionTest {

  @Mock
  private StatsRepository repository;

  @Mock
  private BorrowingInterval borrowingInterval;

  @Mock
  private Subject subject;

  @InjectMocks
  private AvailabilityPredictor predictor;

  @Before
  public void prepareMocksAndPredictor() {
    when(repository.getAverageBorrowingTerm(subject)).thenReturn(borrowingInterval);
  }

  @Test
  public void predictorShouldObtainAverageBorrowingTerm() {
    predictor.getPredictionFor(subject);

    verify(repository).getAverageBorrowingTerm(subject);
  }

  @Test
  public void predictorShouldReturnBorrowingInterval() {
    BorrowingInterval returnedInterval = predictor.getPredictionFor(subject);

    assertThat(returnedInterval).isSameAs(borrowingInterval);
  }

  @Test
  public void userCoefficientShouldBeAppliedToBorrowingInterval() {
    final double USER_COEFFICIENT = 0.85;

    User user = mock(User.class);
    when(repository.getUserCoefficient(user)).thenReturn(USER_COEFFICIENT);

    BorrowingInterval newBorrowingInterval = mock(BorrowingInterval.class);
    when(borrowingInterval.applyUserCoefficient(USER_COEFFICIENT)).thenReturn(newBorrowingInterval);

    assertThat(predictor.getPredictionFor(subject, user)).isSameAs(newBorrowingInterval);
  }

}

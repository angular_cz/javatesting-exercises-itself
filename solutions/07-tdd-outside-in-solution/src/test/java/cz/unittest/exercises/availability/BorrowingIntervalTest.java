package cz.unittest.exercises.availability;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class BorrowingIntervalTest {

  private BorrowingInterval originalInterval;

  @Before
  public void setUp() throws Exception {
    originalInterval = new BorrowingIntervalImpl(2.0, 4.0);
  }

  @Test
  public void userCoefficientDoesNotAffectRangeValue() throws Exception {
    BorrowingInterval newInterval = originalInterval.applyUserCoefficient(1.5);

    assertThat(newInterval.getRange()).isEqualTo(originalInterval.getRange());
  }

  @Test
  public void userCoefficientAffectsAverageValue() throws Exception {
    BorrowingInterval newInterval = originalInterval.applyUserCoefficient(1.5);

    assertThat(newInterval.getAverage()).isEqualTo(3.0);
  }
}

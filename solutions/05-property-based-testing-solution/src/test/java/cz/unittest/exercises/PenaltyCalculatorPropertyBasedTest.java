package cz.unittest.exercises;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assume.*;

import cz.unittest.exercises.domain.PenaltyDefinition;
import cz.unittest.exercises.domain.PublicationType;
import cz.unittest.exercises.penalty.PenaltyCalculator;
import cz.unittest.exercises.penalty.OverdueItem;
import org.junit.Before;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

@RunWith(Theories.class)
public class PenaltyCalculatorPropertyBasedTest {
  private PenaltyCalculator calculator;
  private PenaltyDefinition definition = new PenaltyDefinition();

  @DataPoints
  public static int[] counts = {0, 1, 2, 3, 5, 10, 20, 45};

  @Before
  public void prepareCalculator() {
    calculator = new PenaltyCalculator();
  }

  @Theory
  public void minimalPricePerType(PublicationType type) {
    OverdueItem overdueItem = new OverdueItem(type);

    int result = calculator.calculate(overdueItem);

    assertThat(result).isGreaterThanOrEqualTo(definition.getMinItemPenalty());
  }

  @Theory
  public void maximalPricePerType(PublicationType type) {
    OverdueItem overdueItem = new OverdueItem(type);

    int result = calculator.calculate(overdueItem);

    assertThat(result).isLessThanOrEqualTo(definition.getMaxItemPenalty());
  }

  @Theory
  public void pricePerManyItemsIsSameAsMultipliedPricePerOne(PublicationType type, int n) {
    OverdueItem singleItem = new OverdueItem(type);
    OverdueItem multipleItem = new OverdueItem(type, n);

    int singleItemPrice = calculator.calculate(singleItem);
    int multipleItemsPrice = calculator.calculate(multipleItem);

    assertThat(multipleItemsPrice).isEqualTo(singleItemPrice * n);
  }

  @Theory
  public void priceForTwoTypesTogetherIsSameAsSumOfEachOfThem(PublicationType type1, PublicationType type2) {
    assumeFalse(type1 == type2);

    OverdueItem singleItem1 = new OverdueItem(type1);
    OverdueItem singleItem2 = new OverdueItem(type2);

    List<OverdueItem> multipleItems = new ArrayList<OverdueItem>();
    multipleItems.add(new OverdueItem(type1, 1));
    multipleItems.add(new OverdueItem(type2, 1));

    int singleItemPrice1 = calculator.calculate(singleItem1);
    int singleItemPrice2 = calculator.calculate(singleItem2);
    int multipleItemsPrice = calculator.calculate(multipleItems);

    assertThat(multipleItemsPrice).isEqualTo(singleItemPrice1 + singleItemPrice2);
  }

}

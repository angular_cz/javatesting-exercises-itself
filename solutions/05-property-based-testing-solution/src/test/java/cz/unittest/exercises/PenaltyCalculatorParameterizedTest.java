package cz.unittest.exercises;

import cz.unittest.exercises.domain.PublicationType;
import cz.unittest.exercises.penalty.OverdueItem;
import cz.unittest.exercises.penalty.PenaltyCalculator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.*;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class PenaltyCalculatorParameterizedTest {

  private PublicationType type;
  private int expectedPrice;

  private PenaltyCalculator calculator;

  public PenaltyCalculatorParameterizedTest(PublicationType type, int expectedPrice) {
    this.type = type;
    this.expectedPrice = expectedPrice;
  }

  @Parameters(name = "{index}: expected penalty for type {0} = {1}")
  public static Iterable<Object[]> parametersForPenaltyByType() {
    return Arrays.asList(new Object[][]{
      {PublicationType.DVD, 30},
      {PublicationType.BOOK, 20},
      {PublicationType.MAGAZINE, 10}
    });
  }

  @Before
  public void prepareCalculator() {
    calculator = new PenaltyCalculator();
  }

  @Test
  public void penaltyByType() {
    OverdueItem overdueItem = new OverdueItem(type, 1);

    int price = calculator.calculate(overdueItem);

    assertEquals(expectedPrice, price);
  }
}


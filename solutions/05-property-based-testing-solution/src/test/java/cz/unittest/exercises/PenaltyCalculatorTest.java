package cz.unittest.exercises;

import cz.unittest.exercises.domain.PublicationType;
import cz.unittest.exercises.penalty.PenaltyCalculator;
import cz.unittest.exercises.penalty.OverdueItem;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class PenaltyCalculatorTest {
  private PenaltyCalculator calculator;

  @Before
  public void prepareCalculator() {
    calculator = new PenaltyCalculator();
  }

  @Test
  public void emptyList() {

    List<OverdueItem> overdueItems = Collections.emptyList();
    assertEquals(0, calculator.calculate(overdueItems));
  }

  @Test
  public void penaltyForBook() {
    OverdueItem overdueItem = new OverdueItem(PublicationType.BOOK, 1);

    assertEquals(20, calculator.calculate(overdueItem));
  }

  @Test
  public void penaltyForDVD() {
    OverdueItem overdueItem = new OverdueItem(PublicationType.DVD, 1);

    assertEquals(30, calculator.calculate(overdueItem));
  }

  @Test
  public void penaltyForMagazine() {
    OverdueItem overdueItem = new OverdueItem(PublicationType.MAGAZINE, 1);

    assertEquals(10, calculator.calculate(overdueItem));
  }

  @Test
  public void penaltyForEveryPiece() {
    OverdueItem overdueItem = new OverdueItem(PublicationType.MAGAZINE, 3);

    assertEquals(30, calculator.calculate(overdueItem));
  }

  @Test
  public void penaltyForManyItemsShouldBeSum() {
    List<OverdueItem> overdueItems = new ArrayList<OverdueItem>();

    overdueItems.add(new OverdueItem(PublicationType.MAGAZINE, 1));
    overdueItems.add(new OverdueItem(PublicationType.BOOK, 1));
    overdueItems.add(new OverdueItem(PublicationType.DVD, 2));

    assertEquals(90, calculator.calculate(overdueItems));
  }

}

package cz.unittest.exercises.penalty;

import cz.unittest.exercises.domain.PenaltyDefinition;

import java.util.Collections;
import java.util.List;

public class PenaltyCalculator {

  private static PenaltyDefinition definition = new PenaltyDefinition();

  public int calculate(List<OverdueItem> overdueItems) {
    int sum = 0;

    for (OverdueItem item : overdueItems) {
      sum += calculate(item);
    }

    return sum;
  }

  public int calculate(OverdueItem item) {
    Integer penaltyForType = definition.getPenaltyForType(item.getType());
    return penaltyForType * item.getCount();
  }

}

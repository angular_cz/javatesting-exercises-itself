package cz.unittest.exercises;

import org.joda.time.LocalDate;

public class SubscriptionCalculator {

  private final int basePrice;

  public SubscriptionCalculator(int basePrice) {
    this.basePrice = basePrice;
  }

  public int calculatePrice(User user, Duration duration) {

    int price = basePrice;
    int age = user.getAgeInThisMonth(new LocalDate());

    if (duration == Duration.SIX_MONTHS) {
      price = price / 2;
    }

    if (age <= 25 && age >= 15) {
      price = price / 2;
    }

    if (age >= 70 || age < 15) {
      price = 0;
    }

    return price;
  }

}

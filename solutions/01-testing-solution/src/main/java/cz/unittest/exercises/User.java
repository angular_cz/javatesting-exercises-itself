package cz.unittest.exercises;

import org.joda.time.LocalDate;

public class User {

  private String name;
  private LocalDate dateOfBirth;

  public User(String name, LocalDate dateOfBirth) {
    this.name = name;
    this.dateOfBirth = dateOfBirth;
  }

  public String getName() {
    return name;
  }

  public int getAgeInThisMonth(LocalDate today) {

    int age = today.getYear() -  dateOfBirth.getYear();
    if (dateOfBirth.getMonthOfYear() > today.getMonthOfYear()) {
      age--;
    }

    return age;
  }

}

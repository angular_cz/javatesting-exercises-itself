package cz.unittest.exercises;

import org.joda.time.LocalDate;
import org.junit.Test;

import static org.junit.Assert.*;

public class SubscriptionCalculatorTest {

  @Test
  public void calculatePrice_withAdult_shouldReturnBasePrice() {
    User user = new User("Adult user", new LocalDate(1980, 1, 1));
    SubscriptionCalculator calculator = new SubscriptionCalculator(100);

    int annualPrice = calculator.calculatePrice(user, Duration.ANNUAL);

    assertEquals(100, annualPrice);
  }

  @Test
  public void calculatePrice_withAdultForSixMonths_shouldReturnHalfPrice() {
    User user = new User("Adult user", new LocalDate(1980, 1, 1));
    SubscriptionCalculator calculator = new SubscriptionCalculator(100);

    int sixMonthsPrice = calculator.calculatePrice(user, Duration.SIX_MONTHS);

    assertEquals(50, sixMonthsPrice);
  }

}

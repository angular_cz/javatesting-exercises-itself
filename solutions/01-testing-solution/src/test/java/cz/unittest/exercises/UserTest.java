package cz.unittest.exercises;


import org.joda.time.LocalDate;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserTest {

  @Test
  public void ageOneMonthBeforeBirthDayShouldBeUnchanged() {
    User user = new User("Test user", new LocalDate(1975, 7, 15));
    int age = user.getAgeInThisMonth(new LocalDate(2016, 6, 15));

    assertEquals(40, age);
  }

  @Test
  public void ageOneMonthAfterBirthDayShouldBeHigher() {

    User user = new User("Test user", new LocalDate(1975, 5, 15));
    int age = user.getAgeInThisMonth(new LocalDate(2016, 6, 15));

    assertEquals(41, age);
  }

  @Test
  public void ageInMonthOfBirthShouldBeConsistent() {

    LocalDate today = new LocalDate(2016, 6, 15);

    User hasBirthday = new User("Has birthday today", new LocalDate(1975, 6, 15));
    User alreadyHadBirthday = new User("Had birthday", new LocalDate(1975, 6, 1));
    User willHaveBirthday = new User("Will have birthday", new LocalDate(1975, 6, 30));

    assertEquals(41, hasBirthday.getAgeInThisMonth(today));
    assertEquals(41, alreadyHadBirthday.getAgeInThisMonth(today));
    assertEquals(41, willHaveBirthday.getAgeInThisMonth(today));
  }
}

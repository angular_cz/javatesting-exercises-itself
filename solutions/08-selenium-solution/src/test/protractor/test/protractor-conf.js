var ScreenShotReporter = require('protractor-screenshot-reporter');

exports.config = {
  allScriptsTimeout: 11000,

  specs: [
    'scenarios/**/*.js'
  ],

  capabilities: {
    'browserName': 'chrome'
  },

  baseUrl: 'http://beer-app.angular.cz/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    defaultTimeoutInterval: 30000
  },

  onPrepare: function() {
    jasmine.getEnv().addReporter(new ScreenShotReporter({
      baseDirectory: './protractor-screenshots',
      takeScreenShotsOnlyForFailedSpecs: true
    }));
  }
};

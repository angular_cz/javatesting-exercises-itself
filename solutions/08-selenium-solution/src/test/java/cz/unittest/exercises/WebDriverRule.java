package cz.unittest.exercises;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.junit.rules.ExternalResource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebDriverRule extends ExternalResource{

  private WebDriver driver;

  @Override
  protected void before() throws Throwable {
    ChromeDriverManager.getInstance().setup();

    driver = new ChromeDriver();
  }

  @Override
  protected void after() {
    driver.quit();
  }

  public WebDriver getDriver() {
    return driver;
  }
}

package cz.unittest.exercises;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SubscriptionCalculatorForDiscountsTest extends AbstractSubscriptionCalculatorTest {

  private User retiredUser;
  private User child;
  private User youngUser;

  @Before
  public void prepareUsers() throws Exception {
    retiredUser = new User("Retired user", new LocalDate(1930, 1, 1));
    child = new User("Child user",new LocalDate(2010, 1, 1));
    youngUser = new User("Young user",new LocalDate(2000, 1, 1));
  }

  @Test
  public void calculatePrice_withRetired_shouldBeFree() {
    int price = calculateAnnualPriceFor(retiredUser);

    assertEquals(0, price);
  }

  @Test
  public void calculatePrice_withChild_shouldBeFree(){;
    int price = calculateAnnualPriceFor(child);

    assertEquals(0, price);
  }

  @Test
  public void calculatePrice_withYoung_shouldBeHalf() {
    int price = calculateAnnualPriceFor(youngUser);

    assertEquals(BASE_PRICE / 2, price);
  }

  @Test
  public void calculatePrice_withYoungAndSixMonths_shouldBeQuarter(){
    int price = calculateSixMonthsPriceFor(youngUser);

    assertEquals(BASE_PRICE / 4, price);
  }

}

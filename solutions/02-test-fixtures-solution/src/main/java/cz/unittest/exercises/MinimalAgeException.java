package cz.unittest.exercises;

public class MinimalAgeException extends RuntimeException {

  public MinimalAgeException(int years) {
    super("User must be at least " + years + " years old.");
  }
}

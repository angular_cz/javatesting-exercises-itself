package cz.unittest.exercises;

import org.assertj.core.api.Condition;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

// TODO use own Assertions
import static cz.unittest.exercises.assertions.Assertions.assertThat;
//import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AdvancedAssertJMatchersTest {

  public static final Publication FAVORITE_BOOK = new Publication("ID001CZ", PublicationType.BOOK, "Guide to galaxy", 352);
  public static final User PETR = new User("Petr Novák");
  public static final User PAVEL = new User("Pavel Novák");

  public static final List<Publication> SELECTED_ITEMS = Arrays.asList(
    new Publication("ID001CZ", PublicationType.BOOK, "Guide to galaxy", 352, PETR),
    new Publication("ID002SK", PublicationType.BOOK, "Goodbye and thank you for fishes", 630, PETR),
    new Publication("ID003SK", PublicationType.MAGAZINE, "Ikarie", 52, PAVEL)
  );

  @Test
  public void favoriteBookMeetsBasicCriteria() throws Exception {
    assertThat(FAVORITE_BOOK.getType()).isEqualTo(PublicationType.BOOK);
    assertThat(FAVORITE_BOOK.getPages()).isGreaterThan(200);

    assertThat(FAVORITE_BOOK.getId())
      .startsWith("ID")
      .doesNotEndWith("EN");
  }

  @Test
  public void objectCanBeAssertedByOwnAssertThatMethod() throws Exception {
    assertThat(FAVORITE_BOOK).hasType(PublicationType.BOOK);
  }

  @Test
  public void objectCanBeAssertedByPropertyWithValue() {
    Publication bookWithExpectedValues = new Publication("ID001CZ", PublicationType.BOOK, "Guide to galaxy", 352);

    assertThat(FAVORITE_BOOK).isEqualToComparingFieldByField(bookWithExpectedValues);
  }
  @Test
  public void objectCanBeAssertedByCondition() {
    assertThat(FAVORITE_BOOK).has(type(PublicationType.BOOK));
  }

  public Condition<? super Publication> type(final PublicationType type) {
    return new Condition<Publication>() {
      @Override
      public boolean matches(Publication publication) {
        return publication.getType().equals(type);
      }
    };
  }

  @Test
  public void thereIsNoPosterAmongSelectedItems() {
    assertThat(SELECTED_ITEMS).doNotHave(type(PublicationType.POSTER));
  }

  @Test
  public void allBooksFromSelectedItemsShouldBeReservedForPetr() {
    assertThat(SELECTED_ITEMS)
      .filteredOn(type(PublicationType.BOOK))
      .extracting("reservedFor", User.class)
      .containsOnly(PETR);
  }
}

package cz.unittest.exercises;

public class Publication {

  private String id;
  private PublicationType type;
  private int pages;
  private String name;
  private User reservedFor;

  public Publication(String id, PublicationType type, String name, int pages) {
    this.id = id;
    this.type = type;
    this.pages = pages;
    this.name = name;
  }


  public Publication(String id, PublicationType type, String name, int pages, User reservedFor) {
    this(id, type, name, pages);
    this.reservedFor = reservedFor;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public PublicationType getType() {
    return type;
  }

  public void setType(PublicationType type) {
    this.type = type;
  }

  public int getPages() {
    return pages;
  }

  public void setPages(int pages) {
    this.pages = pages;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public User getReservedFor() {
    return reservedFor;
  }

  public void setReservedFor(User reservedFor) {
    this.reservedFor = reservedFor;
  }

  @Override
  public String toString() {
    return "Publication{" +
      "id='" + id + '\'' +
      ", type=" + type +
      ", name='" + name + '\'' +
      ", pages=" + pages +
      '}';
  }
}
